import { arrayToTree } from "./2.arrayToTree";

describe("arrayToTree", () => {
  it("creates a simple tree", () => {
    const result = arrayToTree(["folder", "subfolder", "file.txt"]);
    const expected = [
      {
        name: "folder",
        children: [
          {
            name: "subfolder",
            children: [
              {
                name: "file.txt"
              }
            ]
          }
        ]
      }
    ];
    expect(result).toEqual(expected);
  });

  it("creates a wood", () => {
    const result = arrayToTree(["folder1", "subfolder1", "file1.txt"], ["folder2", "subfolder2", "file2.txt"]);
    const expected = [
      {
        name: "folder1",
        children: [
          {
            name: "subfolder1",
            children: [
              {
                name: "file1.txt"
              }
            ]
          }
        ]
      },
      {
        name: "folder2",
        children: [
          {
            name: "subfolder2",
            children: [
              {
                name: "file2.txt"
              }
            ]
          }
        ]
      },
    ];
    expect(result).toEqual(expected);
  });

  it("creates a empty tree", () => {
    expect(arrayToTree([])).toEqual([]);
  });
});


 /**
 * CHALLENGE: ARRAY TO TREE
 * @name arrayToTree
 * @description Transform an array into a tree like object.
 *
 * @example Usage:
 * arrayToTree(['folder', 'subfolder', 'file.txt']) //=> [{name: 'folder', children: [ { name: 'subfolder', children: [ {name: 'file.txt'} ]} ]}]
 *
 * @param {Array} paths Array of the items to convert
 *
 * @returns {Array} Returns the tree generated from `paths`
 */
/*export function arrayToTree(...paths) {

  function findLast(source, child4add) {
    if (source && !source.hasOwnProperty("name")) {
      source.name = child4add;
    } else {
      for (let key in source) {
        if (key === 'name') {
          if (source.children) {
            findLast(source.children[0] || source.children, child4add);
          } else {
            source['children'] = [{
              name: child4add
            }]
          }
        }
      }
    }
  }

  let tree = {};
  let resultus = [];

  for (let key in arguments) {
    arguments[key].forEach(currenItem => {
      resultus.push(findLast(tree, currenItem));
    });
  }

  console.log(resultus);
  return [tree];

}*/

