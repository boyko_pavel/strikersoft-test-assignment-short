/**
 * CHALLENGE: ARRAY TO TREE
 * @name arrayToTree
 * @description Transform an array into a tree like object.
 *
 * @example Usage:
 * arrayToTree(['folder', 'subfolder', 'file.txt']) //=> [{name: 'folder', children: [ { name: 'subfolder', children: [ {name: 'file.txt'} ]} ]}]
 *
 * @param {Array} paths Array of the items to convert
 *
 * @returns {Array} Returns the tree generated from `paths`
 */
export function arrayToTree (...paths) {
  const wood = [];
  let seedling, tree, probablyTree;

  for (const seeds of arguments) {
    seedling = {};
    for (const seed of seeds) {
      probablyTree = growTree(seedling, seed);
      probablyTree && (tree = probablyTree);
    }
    tree && wood.push(tree);
  }

  function growTree (parent, child) {
    if (parent && !parent.hasOwnProperty('name')) {
      parent.name = child;
    } else {
      for (const key in parent) {
        if (key === 'name') {
          if (parent.children) {
            growTree(parent.children[0] || parent.children, child);
          } else {
            parent['children'] = [
              {
                name: child
              }
            ];
            return seedling;
          }
        }
      }
    }
  }

  return wood;
}
