/**
 * CHALLENGE MAX CHAR
 * @name maxChar
 * @description Get the most used char in a sentence.
 *
 * @example Usage:
 * maxChar('hello') //=> { count: 2, char: 'l' }
 *
 * @param {String} str The string to be checked
 *
 * @returns {Object} Returns character most used in the string.
 */
export function maxChar (str) {
  let [count, char, statistics] = [0, '', {}];

  str.split('').forEach((letter) => {
    letter in statistics ? statistics[letter] += 1 : statistics[letter] = 1;
  });

  for (let key in statistics) {
    if (count < statistics[key]) {
      [count, char] = [statistics[key], key];
    }
  }

  return {count, char};
}
